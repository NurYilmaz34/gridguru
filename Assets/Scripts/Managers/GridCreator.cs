﻿using UnityEngine;

public class GridCreator : MonoBehaviour
{
    private int grid_width;
    private int grid_height;

    #region Singleton
    public static GridCreator instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    #endregion

    private int SetGridLength()
    {
        grid_width = GameManager.instance.currentGridColumn;
        grid_height = GameManager.instance.currentGridRow;

        return grid_width * grid_height;
    }

    public void CreateGrid()
    {
        DataCreator.instance.cells = new Cell[SetGridLength()];
        int currentCellId = 0;
        for (int i = 0; i < grid_height; i++)
        {
            for (int j = 0; j < grid_width; j++)
            {
                DataCreator.instance.CreateCellDatas(i, j);
                Cell cell = CellPool.Instance.Get();
                SetTransformInformation(cell.transform, new Vector3(j - GetGridOffset(grid_width-1), i - GetGridOffset(grid_height-1), grid_width*2), GameManager.instance.cellContainer, true);
                cell.name = "cell_" + currentCellId.ToString();
                DataCreator.instance.cells[currentCellId] = cell;
                DataCreator.instance.SetCellData(currentCellId);
                currentCellId++;
            }
        }
    }

    private float GetGridOffset(int gridLength)
    {
        return gridLength * 0.5f;
    }

    public void SetTransformInformation(Transform obj, Vector3 coordinate, Transform container, bool activate)
    {
        obj.position = coordinate;
        obj.parent = container;
        obj.gameObject.SetActive(activate);
    }

    public void DestructorCell()
    {
        for (int i = 0; i < DataCreator.instance.cells.Length; i++)
        {
            DataCreator.instance.cells[i].xObject.SetActive(false);
            SetTransformInformation(DataCreator.instance.cells[i].transform, Vector3.zero, null, false);
            CellPool.Instance.ReturnToPool(DataCreator.instance.cells[i]);
        }
    }
}
