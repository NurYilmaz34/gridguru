﻿using UnityEngine;
using System.Linq;
using GameGuru.Data;

public class DataCreator : MonoBehaviour
{
    [HideInInspector] public Cell[] cells;
    [HideInInspector] public CellData[] cellDatas;

    #region Singleton
    public static DataCreator instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    #endregion

    public void CreateCellDatas(int row, int column)
    {
        cellDatas = new CellData[GetGridLength()];
        for (int i = 0; i < GetGridLength(); i++)
        {
            cellDatas[i] = new CellData(i, row, column, false);
        }
    }

    public void SetCellData(int i)
    {
        cells[i].CellData = cellDatas.First(lstData => lstData.Id == i);
    }

    private int GetGridLength()
    {
        int length = GameManager.instance.currentGridRow * GameManager.instance.currentGridColumn;
        return length;
    }
}
