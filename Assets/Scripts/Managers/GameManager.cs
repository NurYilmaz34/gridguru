﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [HideInInspector] public int currentGridRow;
    [HideInInspector] public int currentGridColumn;

    public Transform cellContainer;

    public List<Cell> deActivateCellsList = new List<Cell>();

    #region Singleton
    public static GameManager instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    #endregion

    public Cell GetDesiredCell(int row, int column)
    {
        for (int i = 0; i < DataCreator.instance.cells.Length; i++)
        {
            if (DataCreator.instance.cells[i].CellData.rowNo == row && DataCreator.instance.cells[i].CellData.columnNo == column)
            {
                return DataCreator.instance.cells[i];
            }
        }
        return DataCreator.instance.cells[0];
    }

    public void CheckCombo(int currentRow, int currentColumn)
    {
        GeneralControl(currentRow, currentColumn);

        if (deActivateCellsList.Count >= 3) DestroyXCombo();
        else deActivateCellsList.Clear();
    }

    public void GeneralControl(int currentRow, int currentColumn)
    {
        GetColumnRightControl(currentRow, currentColumn);
        GetColumnLeftControl(currentRow, currentColumn);
        GetRowUpControl(currentRow, currentColumn);
        GetRowDownControl(currentRow, currentColumn);
    }

    public void GetColumnRightControl(int currentRow, int currentColumn)
    {
        for (int i = currentColumn+1; i < currentGridColumn; i++)
        {
            if (GetDesiredCell(currentRow, i).CellData.xActiveState && !deActivateCellsList.Contains(GetDesiredCell(currentRow, i)))
            {
                deActivateCellsList.Add(GetDesiredCell(currentRow, i));
                GeneralControl(currentRow, i);
            } 
            else return;
        }
    }
    public void GetColumnLeftControl(int currentRow, int currentColumn)
    {
        for (int i = currentColumn-1; i >= 0; i--)
        {
            if (GetDesiredCell(currentRow, i).CellData.xActiveState && !deActivateCellsList.Contains(GetDesiredCell(currentRow, i)))
            {
                deActivateCellsList.Add(GetDesiredCell(currentRow, i));
                GeneralControl(currentRow, i);
            } 
            else return;
        }
    }
    public void GetRowUpControl(int currentRow, int currentColumn)
    {
        for (int i = currentRow+1; i < currentGridRow; i++)
        {
            if (GetDesiredCell(i, currentColumn).CellData.xActiveState && !deActivateCellsList.Contains(GetDesiredCell(i, currentColumn)))
            {
                deActivateCellsList.Add(GetDesiredCell(i, currentColumn));
                GeneralControl(i, currentColumn);
            }
            else return;
        }
    }
    public void GetRowDownControl(int currentRow, int currentColumn)
    {
        for (int i = currentRow - 1; i >= 0; i--)
        {
            if (GetDesiredCell(i, currentColumn).CellData.xActiveState && !deActivateCellsList.Contains(GetDesiredCell(i, currentColumn)))
            {
                deActivateCellsList.Add(GetDesiredCell(i, currentColumn));
                GeneralControl(i, currentColumn);
            } 
            else return;
        }
    }

    public void DestroyXCombo()
    {
        for (int i = 0; i < deActivateCellsList.Count; i++)
        {
            deActivateCellsList[i].CellData.xActiveState = false;
            deActivateCellsList[i].xObject.SetActive(false);
        }
        deActivateCellsList.Clear();
        UIManager.instance.GetCurrentComboValues(UIManager.instance.ComboUp());
        UIManager.instance.CheckRestartGame();
    }
}
