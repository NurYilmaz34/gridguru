﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text comboText;
    public Text targetText;
    public Text inputText;

    public InputField input;

    public Button confirmButton;
    public Button restartButton;

    public CanvasGroup GamePanel;
    public CanvasGroup inputPopUp;
    public CanvasGroup RestartPanel;

    private int comboCount = 1;
    private int inputGridLength = 0;
    private int targetComboCount = 5;
    
    #region Singleton
    public static UIManager instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        GetStartComboTargetAndCurrentValue();
    }
    #endregion

    private void Start()
    {
        confirmButton.onClick.AddListener(PrepareGamePlay);
        restartButton.onClick.AddListener(GetRestartGame);
    }

    public void SetPanelAlpha(CanvasGroup panel, float value)
    {
        panel.alpha = value;
    }

    public bool CheckGridLength()
    {
        if (int.Parse(input.text) < 2 || int.Parse(input.text) > 10)
        {
            return false;
        }
        else
            return true;
    }

    public void GetGridLength()
    {
        inputGridLength = int.Parse(input.text);
        GameManager.instance.currentGridRow = inputGridLength;
        GameManager.instance.currentGridColumn = inputGridLength;
    }

    public void PrepareGamePlay()
    {
        if (inputText.text == "") return;
        if (CheckGridLength())
        {
            SetPanelAlpha(GamePanel, 1f);
            inputPopUp.gameObject.SetActive(false);
            GetGridLength();
            GridCreator.instance.CreateGrid();
        }
        else
        {
            input.text = null;
            return;
        }
    }

    public void GetStartComboTargetAndCurrentValue()
    {
        SetPanelAlpha(GamePanel, 0f);
        comboText.text = "Combo : ";
        targetText.text = "Target : " + targetComboCount;
    }

    public void GetCurrentComboValues(int comboCount)
    {
        comboText.text = "Combo : " + comboCount;
    }

    public int ComboUp()
    {
        return comboCount++;
    } 

    public void CheckRestartGame()
    {
        if (comboCount == (targetComboCount+1))
        {
            input.text = null;
            comboCount = 1;
            SetPanelAlpha(GamePanel, 0f);
            GetStartComboTargetAndCurrentValue();
            GridCreator.instance.DestructorCell();
            RestartPanel.gameObject.SetActive(true);
        }
    }

    public void GetRestartGame()
    {
        SetPanelAlpha(GamePanel, 0f);
        RestartPanel.gameObject.SetActive(false);
        inputPopUp.gameObject.SetActive(true);
    }
}
