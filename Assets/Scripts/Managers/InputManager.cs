﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public Camera cam;
    private Cell dummyCell;

    [HideInInspector] public RaycastHit2D hit;

    [HideInInspector] public bool isActiveGame = true;

    [HideInInspector] public PointerEventData eventData;

    #region Singleton
    public static InputManager instance = null;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    #endregion


    public void OnPointerDown(PointerEventData _eventData)
    {
        eventData = _eventData;
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10;
        Vector3 screenPos = cam.ScreenToWorldPoint(mousePos);
        hit = Physics2D.Raycast(screenPos, Vector2.zero, Mathf.Infinity);

        if (hit.transform.CompareTag("cell"))
        {
            dummyCell = hit.transform.transform.GetComponent<Cell>();
            dummyCell.CellData.xActiveState = true;
            dummyCell.xObject.SetActive(true);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        int currentRow = dummyCell.CellData.rowNo;
        int currentColumn = dummyCell.CellData.columnNo;

        GameManager.instance.CheckCombo(currentRow, currentColumn);
    }
}
