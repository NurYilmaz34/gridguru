﻿using UnityEngine;
using GameGuru.Data;

public class Cell : MonoBehaviour
{
    public CellData CellData { get; set; }

    public GameObject xObject
    {
        get
        {
            return transform.GetChild(0).gameObject;
        }
    }

    private void Start()
    {
        SetOrderInLayer();
    }

    private void SetOrderInLayer()
    {
        xObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
    }
}
