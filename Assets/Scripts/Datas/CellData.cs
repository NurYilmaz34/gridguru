﻿namespace GameGuru.Data
{
    public class CellData
    {
        public int Id { get; set; }
        public int rowNo { get; set; }
        public int columnNo { get; set; }
        public bool xActiveState { get; set; }

        public CellData(int id, int row, int column, bool isActive)
        {
            Id = id;
            rowNo = row;
            columnNo = column;
            xActiveState = isActive;
        }
    }
}
